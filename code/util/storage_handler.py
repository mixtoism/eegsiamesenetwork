import io
import pathlib
import logging

logger = logging.getLogger(__name__)


class StorageHandler:
    @staticmethod
    def load_object(filename, serializer):
        logger.info(
            "Loading object {} with serializer {}".format(filename, serializer.name)
        )
        buff = io.BytesIO()
        with pathlib.Path(filename).open("rb") as fd:
            buff = fd.read()
        return serializer.deserialize(buff)

    @staticmethod
    def save_object(filename, content, serializer):
        logger.info(
            "Saving object {} with serializer {}".format(filename, serializer.name)
        )
        path = pathlib.Path(filename)
        path.parent.mkdir(parents=True, exist_ok=True)
        with path.open("wb") as fd:
            buff = serializer.serialize(content)
            fd.write(buff.read())

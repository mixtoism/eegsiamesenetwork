import io

import scipy.io as sio
import pickle


class AbstractSerializer:
    name = "abstract"

    @staticmethod
    def serialize(obj):
        ...

    @staticmethod
    def deserialize(buff):
        ...


class PickleSerializer(AbstractSerializer):
    name = "PickleSerializer"

    @staticmethod
    def serialize(obj):
        buff = io.BytesIO()
        pickle.dump(obj, buff)
        buff.seek(0)
        return buff
    
    @staticmethod
    def deserialize(buff):
        return pickle.loads(buff)

class MatlabSerializer(AbstractSerializer):
    name = "MatlabSerializer"

    @staticmethod
    def serialize(obj):
        buff = io.BytesIO()
        sio.savemat(buff, obj)
        buff.seek(0)
        return buff

    def deserialize(buff):
        return sio.loadmat(buff)
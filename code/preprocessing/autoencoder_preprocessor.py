from util.serializers import PickleSerializer
from util.storage_handler import StorageHandler
import librosa
import numpy as np
import pathlib


class AutoencoderPreprocessor:
    def __init__(
        self,
        sample_lenght,
        stimuli,
        input_path,
        output_path,
        output_path_test,
        training_session,
    ):
        self.sample_lenght = sample_lenght
        self.stimuli = stimuli
        self.input_path = pathlib.Path(input_path)
        self.output_path = pathlib.Path(output_path)
        self.output_path_test = pathlib.Path(output_path_test)
        self.training_session = training_session

    def get_file_destination(self, file):
        if int(file.split("_")[1][1:]) == self.training_session:
            return self.output_path
        return self.output_path_test

    def get_event_indxs(self, file):
        event_indxs = []
        for idx, event in enumerate(file["events"]):
            conditions = [
                (
                    event[2][field] == value
                    if event[2].dtype.fields.get(field)
                    else False
                )
                for field, value in self.stimuli.items()
            ]

            if all(conditions):
                event_indxs.append(idx)
        return event_indxs

    def get_event_samples(self, file, event_idxs):
        samples = []
        for event in event_idxs:
            start = np.squeeze(file["events"][event][0])
            end = np.squeeze(file["events"][event][1])
            sample_idxs = (file["recording"][:, -1] > start) & (
                file["recording"][:, -1] < end
            )
            samples.append(file["recording"][sample_idxs, 2:-1])
        return samples

    def cut_samples(self, samples, sample_lenght):
        window = np.hanning(sample_lenght).reshape(-1, 1)
        all_frames = []
        for sample in samples:
            frames = librosa.util.frame(
                sample.T, sample_lenght, int(sample_lenght * 0.8)
            )
            windowed_frames = window * frames
            for wframe in windowed_frames:
                all_frames.append(wframe)
        return all_frames

    def preprocess_all_files(self):
        for file, name in self.yield_files():
            dest = self.get_file_destination(name)
            event_idxs = self.get_event_indxs(file)
            samples = self.get_event_samples(file, event_idxs)
            cutted_samples = self.cut_samples(samples, self.sample_lenght)
            for idx, cutted_sample in enumerate(cutted_samples):
                filename = dest / f"{name}_{idx:04}.pickle"
                StorageHandler.save_object(filename, cutted_sample, PickleSerializer)

    def yield_files(self):
        for file in self.input_path.iterdir():
            yield StorageHandler.load_object(file, PickleSerializer), file.stem

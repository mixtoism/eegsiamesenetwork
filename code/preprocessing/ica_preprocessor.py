import pathlib
import logging

import mne
from util.storage_handler import StorageHandler
from util.serializers import PickleSerializer, MatlabSerializer

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class ICAPreprocessor:
    def __init__(self, input, output):
        self.input_path = pathlib.Path(input)
        self.output_path = pathlib.Path(output)
        self.output_path.mkdir(parents=True, exist_ok=True)

    def preprocess_all_files(self):
        for file, name in self.yield_files():
            # SELECT ONLY THE EEG CHANNELS TAKING INTO ACCOUNT
            # COUNTER INTERPOLATED F3 FC5 AF3 F7 T7 P7 O1 O2 P8 T8 F8 AF4 FC6 F4 UNIX_TIMESTAMP
            recording = file["recording"][:, 2:-1]
            info = mne.create_info(
                "F3 FC5 AF3 F7 T7 P7 O1 O2 P8 T8 F8 AF4 FC6 F4".split(),
                sfreq=256,
                ch_types="eeg",
            )
            raw_array = mne.io.RawArray(recording.T, info)
            ica = mne.preprocessing.ICA(n_components=14)
            ica.fit(raw_array.filter(l_freq=0.4, h_freq=None))
            preprocessed_array = ica.apply(raw_array.filter(l_freq=0.4, h_freq=None))
            preprocessed_recording = preprocessed_array._data.T
            file["recording"][:, 2:-1] = preprocessed_recording
            StorageHandler.save_object(name, file, PickleSerializer)

    def yield_files(self):
        for file in self.input_path.iterdir():
            yield StorageHandler.load_object(file, MatlabSerializer), file.stem

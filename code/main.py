import logging

from preprocessing.autoencoder_preprocessor import AutoencoderPreprocessor

logging.basicConfig(level=logging.INFO)


import fire
import wandb
from wandb.keras import WandbCallback
import tensorflow as tf

from network.lpdistancelayer import LPDistanceLayer
from network.eegautoencoder_layer import EEGAutoEncoderLayer
from network.siamese_model import SiameseNetwork
from network.backbone import EEGEmbeddingBackbone
from network.eegautoencoder_layer import EEGAutoEncoderLayer
from preprocessing.ica_preprocessor import ICAPreprocessor


def ica_cleansing(input_path, output_path):
    preprocessor = ICAPreprocessor(input_path, output_path)
    preprocessor.preprocess_all_files()


def prepare_autoencoder_dataset(
    sample_lenght,
    stimuli,
    input_path,
    output_path,
    output_path_test,
    training_session=1,
):
    # CUT pickle files in segments only from first session
    # save
    preprocessor = AutoencoderPreprocessor(
        sample_lenght,
        stimuli,
        input_path,
        output_path,
        output_path_test,
        training_session,
    )
    preprocessor.preprocess_all_files()


def prepare_siamese_dataset(nshots, sample_lenght, stimuli):
    # CUT PICKLE files in segments from first and second session
    # PREPARE $nshot triplets per user
    # save
    ...


def train_autoencoder():
    wandb.init(project="eeg-autoencoder", entity="pargon")
    autoencoder = tf.keras.Model(EEGAutoEncoderLayer(chan_in=14))
    autoencoder.fit(data, callbacks=[WandbCallback()])
    autoencoder.save("embedding_layer.h5")


def train_siamese_network():
    wandb.init(project="eeg-auth-siamese-network", entity="pargon")
    autoencoder = tf.keras.models.load_model("embedding_layer.h5")
    network = SiameseNetwork(
        EEGEmbeddingBackbone,
        output_layer=LPDistanceLayer,
        margin=0.5,
        backbone_class_kwargs={"autoencoder": autoencoder},
        output_layer_kwargs={"p": 2},
    )
    network.fit(data, labels)


def test_siamese_network():
    ...


def test_autoencoder():
    ...


if __name__ == "__main__":
    fire.Fire()

import tensorflow as tf
from network.lpdistancelayer import LPDistanceLayer


class SiameseNetwork(tf.keras.Model):
    def __init__(
        self,
        backbone_class,
        output_layer=LPDistanceLayer,
        margin=0.5,
        backbone_class_kwargs=dict(),
        output_layer_kwargs={"p": 2},
    ):
        self.backbone = backbone_class(**backbone_class_kwargs)
        self.output_layer = output_layer(**output_layer_kwargs)
        self.loss_tracker = tf.keras.metrics.Mean(name="loss")
        self.margin = margin

    def call(self, inputs):
        features = []
        for idx, input in enumerate(inputs):
            features[idx] = self.backbone(input)
        return self.output_layer(*features)

    def train_step(self, data):
        with tf.GradientTape() as tape:
            loss = self._compute_loss(data)
        gradients = tape.gradient(loss, self.siamese_network.trainable_weights)

        self.optimizer.apply_gradients(
            zip(gradients, self.siamese_network.trainable_weights)
        )

        self.loss_tracker.update_state(loss)
        return {"loss": self.loss_tracker.result()}

    def test_step(self, data):
        loss = self._compute_loss(data)

        self.loss_tracker.update_state(loss)
        return {"loss": self.loss_tracker.result()}

    def _compute_loss(self, data):
        ap_distance, an_distance = self.call(data)

        loss = ap_distance - an_distance
        loss = tf.maximum(loss + self.margin, 0.0)
        return loss

    @property
    def metrics(self):
        return [self.loss_tracker]
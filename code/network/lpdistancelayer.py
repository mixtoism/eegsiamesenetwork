import tensorflow as tf


class LPDistanceLayer(tf.keras.layers.Layer):
    def __init__(self, p=2):
        self.p = p

    def call(self, anchor, positive, negative):
        ap_distance = tf.reduce_sum(tf.pow(anchor - positive, self.p), -1)
        an_distance = tf.reduce_sum(tf.pow(anchor - negative, self.p), -1)
        return (ap_distance, an_distance)

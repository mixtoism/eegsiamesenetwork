import tensorflow as tf


class EEGAutoEncoderLayer(tf.keras.layers.Layer):
    def define_encoder(self):
        self.encoder[0] = tf.keras.layers.Conv1D(
            32, (3,), activation="relu", padding="same"
        )
        self.encoder[1] = tf.keras.layers.MaxPooling1D((2,), padding="same")
        self.encoder[2] = tf.keras.layers.Conv1D(
            32, (3,), activation="relu", padding="same"
        )
        self.encoder[3] = tf.keras.layers.MaxPooling1D((2,), padding="same")

    def define_decoder(self, chan_in):
        self.decoder[0] = tf.keras.layers.Conv1DTranspose(
            32, (3,), strides=2, activation="relu", padding="same"
        )
        self.decoder[1] = tf.keras.layers.Conv1DTranspose(
            32, (3,), strides=2, activation="relu", padding="same"
        )
        self.decoder[2] = tf.keras.layers.Conv1D(
            chan_in, (3,), activation="sigmoid", padding="same"
        )

    def __init__(self, chan_in=14):
        self.define_encoder()
        self.define_decoder(chan_in)

    def call(self, input):
        return self.decode(self.encode(input))

    def encode(self, input):
        res = input
        for step in self.encoder:
            res = step(res)
        return res

    def decode(self, input):
        res = input
        for step in self.decoder:
            res = step(res)
        return res
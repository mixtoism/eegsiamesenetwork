import tensorflow as tf


class EEGEmbeddingBackbone(tf.keras.layers.Layer):
    def __init__(self, autoencoder):
        self.autoencoder = autoencoder
        self.w0 = tf.keras.layers.Dense(500, activation="relu", use_bias=True)
        self.w1 = tf.keras.layers.Dense(30, activation="relu")

    def call(self, inputs):
        encoded = self.autoencoder.encode(inputs)
        return self.w1(self.w0(encoded))
